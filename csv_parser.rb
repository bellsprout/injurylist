require 'csv'
require_relative './injury'

class Parser
  def initialize
    @output = ''
    @current_weyr = ''
    @current_wing = ''
  end

  def parse_lines()
    CSV.foreach('./list.csv', {:headers => true, :header_converters => :symbol}) do |row|
      update_current(row)
      output_line = initialize_line(row)

      # Determine rider/handler injury
      min = row[:min_rider_injury]
      max = row[:max_rider_injury]
      name = row[:rider_name]
      species = 'human'
      injury = get_injury_level(min, max, name, species)
      output_line += "\n#{name}: #{injury}"

      # Determine first bonded injury
      min = row[:min_bond_1_injury]
      max = row[:max_bond_1_injury]
      name = row[:bond_1_name]
      species = row[:bond_species]
      injury = get_injury_level(min, max, name, species)
      output_line += "\n#{name}: #{injury}"

      # Determine second bonded injury if applicable
      if row[:bond_2_name]
        min = row[:min_bond_2_injury]
        max = row[:max_bond_2_injury]
        name = row[:bond_2_name]
        bond_injury = get_injury_level(min, max, name, species)
        output_line += "\n#{name}: #{bond_injury}"
      end

      @output += output_line + "\n\n"
    end

    # Write to both file and stdout in order to double-check
    File.write('./output.txt', @output)
    puts @output
  end

  def update_current(row)
    weyr = row[:weyr]
    wing = row[:wing]

    if weyr != @current_weyr
      @current_weyr = weyr
      @output += "[center][size=10][b]#{weyr} Weyr[/b][/size][/center]\n"
    end
    if wing != @current_wing
      @current_wing = wing
      @output += "[center][b][u]#{wing}[/u][/b][/center]\n"
    end
  end

  def initialize_line(row)
    line = "[b]#{row[:rider_name]} of #{row[:bond_1_color]} #{row[:bond_1_name]}"
    if row[:bond_2_name]
     line += " and #{row[:bond_2_color]} and #{row[:bond_2_name]}"
    end
    line += ":[/b] "
    return line
  end
end

parser = Parser.new
parser.parse_lines()
