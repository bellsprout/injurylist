require_relative './data'

LEVEL_SET = []

def get_level_set
  if LEVEL_SET.length == 0
    35.times do
      LEVEL_SET << 'None'
    end
    20.times do
      LEVEL_SET << 'Trivial'
    end
    23.times do
      LEVEL_SET << 'Mild'
    end
    15.times do
      LEVEL_SET << 'Moderate'
    end
    5.times do
      LEVEL_SET << 'Major'
    end
    3.times do
      LEVEL_SET << 'Permanent'
    end
    2.times do
      LEVEL_SET << 'Fatal'
    end
  end

  return LEVEL_SET
end

def get_injury_level(min, max, name, species)
  min = normalize_values(min)
  max = normalize_values(max)

  min_level = get_level_by_name(min)
  max_level = get_level_by_name(max)

  if min_level == max_level
    injury = min_level
  else
    injury = roll_for_injury()
    while (injury > max_level || injury < min_level) do
      puts "#{name}: #{injury.level}"
      injury = roll_for_injury()
    end
  end

  if species
    injury.roll_details(species.downcase)
  end

  return injury
end

def normalize_values(text)
  if !text || text == 'DNR' || text == 'N/A'
    text = 'None'
  end
  if text == 'Minor'
    text = 'Mild'
  end
  return text
end

def get_level_by_name(name)
  level_name = Object.const_get(name)
  level_name.new
end

def roll_for_injury
  levels = get_level_set();
  level = levels.sample
  injury = get_level_by_name(level)
  return injury
end

class Injury
  attr_accessor :level, :num
  include Comparable

  def initialize
    @rerolls = 0
    @reroll_threshold = 0
    @injuries = []
  end

  def <=>(other)
    self.num <=> other.num
  end

  def roll_details(species)
    return '' if @num == 1

    body_parts = BODY_PARTS[species]
    injury_types = INJURY_TYPES[@level]

    part = body_parts.sample
    type = injury_types.sample

    @injuries << [part, type]

    rerolls = @rerolls
    while rerolls > 0
      roll = 1 + rand(100)
      if roll <= @reroll_threshold
        part = body_parts.sample
        type = injury_types.sample

        @injuries << [part, type]
        rerolls -= 1
      else
        # We only keep rolling until we fail one; exploding dice and all
        rerolls = 0
      end
    end
  end

  def to_s
    output = ''
    if @injuries.length > 0
      injury_lines = []
      @injuries.each do |injury|
        line = "#{injury[1]}"
        if injury[0]
          line += " => #{injury[0]}"
        end
        injury_lines << line
      end
      output = injury_lines.join(', ')
    end
    output += " (#{@level})"
  end
end

class None < Injury
  def initialize
    super
    @level = 'None'
    @num = 1
  end
end

class Trivial < Injury
  def initialize
    super
    @level = 'Trivial'
    @num = 2
  end

  def roll_details(species)
    return '' if @num == 1

    injury_types = INJURY_TYPES[@level]
    type = injury_types.sample

    @injuries << [nil, type]
  end
end

class Mild < Injury
  def initialize
    super
    @level = 'Mild'
    @num = 3
  end
end

class Moderate < Injury
  def initialize
    super
    @level = 'Moderate'
    @num = 4
    @rerolls = 1
    @reroll_threshold = 20
  end
end

class Major < Injury
  def initialize
    super
    @level = 'Major'
    @num = 5
    @rerolls = 3
    @reroll_threshold = 50
  end
end

class Permanent < Injury
  def initialize
    super
    @level = 'Permanent'
    @num = 6
    @rerolls = 6
    @reroll_threshold = 60
  end
end

class Fatal < Injury
  def initialize
    super
    @level = 'Fatal'
    @num = 7
    @rerolls = 2
    @reroll_threshold = 95
  end
end
